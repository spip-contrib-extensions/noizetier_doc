<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/plugonet/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'noizetier_doc_description' => 'Ce plugin permet de lister depuis l\'espace privé de SPIP l\'ensemble des noisettes à disposition sur votre site.',
	'noizetier_doc_slogan' => 'Des noisettes, oui, documentées, c\'est mieux!'
);
